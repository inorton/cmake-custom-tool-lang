include(CMakeLanguageInformation)
set(CMAKE_CustomTool_LINKER_PREFERENCE_PROPAGATES 0 CACHE INTERNAL "")
set(CMAKE_CustomTool_USE_RESPONSE_FILE_FOR_OBJECTS 0 CACHE INTERNAL "")
set(CMAKE_CustomTool_LINK_EXECUTABLE "CustomTool_NO_LINK_EXECUTABLE" CACHE INTERNAL "")
set(CMAKE_CustomTool_CREATE_SHARED_LIBRARY "CustomTool_NO_CREATE_SHARED_LIBRARY" CACHE INTERNAL "")
set(CMAKE_CustomTool_CREATE_SHARED_MODULE "CustomTool_NO_CREATE_SHARED_MODULE" CACHE INTERNAL "")
set(CMAKE_CustomTool_CREATE_STATIC_LIBRARY "CustomTool_NO_CREATE_STATIC_LIBRARY" CACHE INTERNAL "")
set(CMAKE_CustomTool_OUTPUT_EXTENSION .cust_tool CACHE INTERNAL "")

# see - https://github.com/Kitware/CMake/blob/master/Modules/CMakeCInformation.cmake
# possible things we can use are
# <DEFINES> => flags added with target_compile_definitions()
# <INCLUDES> => dirs added with target_include_directores() with each prefixed with ${CMAKE_INCLUDE_FLAG_<LANG>})
# <FLAGS>  => flags added with target_compile_options()
# <SOURCE> source file
# <OBJECT> => ${CMAKE_CURRENT_BINARY_DIR}/<SOURCE>${CMAKE_CustomTool_OUTPUT_EXTENSION}

# with add_custom_tool we will use <FLAGS> to contain our command line

set(CMAKE_CustomTool_COMPILE_OBJECT
    # this is a list, builds execute each thing in it in order
    # "${CMAKE_COMMAND} -E echo <FLAGS>"
  "${CMAKE_COMMAND} -E chdir <FLAGS>"   # first FLAGS element is the folder to run inside
  "${CMAKE_COMMAND} -E touch <OBJECT>"  #
  CACHE INTERNAL "")


set(CMAKE_CustomTool_INFORMATION_LOADED 1 CACHE INTERNAL "")
