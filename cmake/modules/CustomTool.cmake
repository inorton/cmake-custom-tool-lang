include(${CMAKE_CURRENT_LIST_DIR}/CMakeCustomToolCompiler.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/CMakeCustomToolInformation.cmake)

function(add_custom_tool)
  set(_arg_NAME ${ARGV0})
  set(_arg_WORKING_DIRECTORY) # change to this folder before running
  set(_arg_TARGET_LANGUAGE) # mark the output file as this langauge, default to C
  set(_arg_INPUTS)  # files or other custom_tool targets we depend on
  set(_arg_OUTPUTS) # list of files the target produces
  set(_arg_COMMAND) # command line to run
  cmake_parse_arguments(
    PARSE_ARGV 1
    _arg
    "" # toggles
    "WORKING_DIRECTORY;TARGET_LANGUAGE" # single value options
    "COMMAND;INPUTS;OUTPUTS" # multi-value args
    )
  if (NOT _arg_TARGET_LANGUAGE)
    set(_arg_TARGET_LANGUAGE C)
  endif()

  if (NOT _arg_WORKING_DIRECTORY)
    message(FATAL_ERROR "WORKING_DIRECTORY required")
  endif()

  set(libname custom_tool_${_arg_NAME})
  set(targetname ${_arg_NAME})

  message(STATUS "added custom tool: target=${targetname}")
  message(STATUS "${targetname} lang=${_arg_TARGET_LANGUAGE}")
  message(STATUS "${targetname} inputs=${_arg_INPUTS}")
  message(STATUS "${targetname} outputs=${_arg_OUTPUTS}")
  message(STATUS "${targetname} command=${_arg_COMMAND}")

  if(NOT _arg_INPUTS)
    set(_arg_INPUTS "${CMAKE_CURRENT_BINARY_DIR}/custom_tool_${_arg_NAME}.custom")
    file(WRITE "${_arg_INPUTS}" "")
  endif()

  add_library(${libname} OBJECT "")
  # our first flag is a chdir argument
  target_compile_options(${libname} PRIVATE ${_arg_WORKING_DIRECTORY})
  target_compile_options(${libname} PRIVATE ${_arg_COMMAND})

  add_custom_target(${targetname})
  add_dependencies(${targetname} ${libname})

  if (_arg_INPUTS)
    foreach(infile ${_arg_INPUTS})
      if(TARGET ${infile})
        add_dependencies(${libname} ${infile})  # a target we depend on

        # if the target sets CUSTOM_TOOL_OUTPUTS we consume those
        get_target_property(depend_outs ${infile} OUTPUTS)
        if(depend_outs)
          foreach(depend_file ${depend_outs})
            message(STATUS "${targetname} depends on ${depend_file} from target ${infile}")
            target_sources(${libname} ${depend_file})
          endforeach()
        endif()
      else()
        if(NOT infile_lang)
          set_source_files_properties(${infile}
            PROPERTIES
            LANGUAGE CustomTool)
        endif()

        get_source_file_property(infile_lang ${infile} LANGUAGE)
        message(STATUS "${_arg_NAME} depends on ${infile} lang=${infile_lang}")
        target_sources(${libname} PRIVATE ${infile})

      endif()
    endforeach()
  endif()

  if(_arg_OUTPUTS)
    set_target_properties(${targetname}
      PROPERTIES
        OUTPUTS "${_arg_OUTPUTS}")
    foreach(outfile ${_arg_OUTPUTS})
      set_source_files_properties(${outfile}
        PROPERTIES
          GENERATED TRUE)
      if(_arg_TARGET_LANGUAGE)
        message(STATUS "${targetname} makes ${outfile} LANG=${_arg_TARGET_LANGUAGE}")
        set_source_files_properties(${outfile}
          PROPERTIES
            LANGUAGE ${_arg_TARGET_LANGUAGE})
      endif()
    endforeach()
  endif()
endfunction()

function(target_sources_custom_outputs)
  add_dependencies(${ARGV0} ${ARGV1})
  target_sources(${ARGV0} PRIVATE $<TARGET_PROPERTY:${ARGV1},OUTPUTS>)
endfunction()
