cmake_minimum_required(VERSION 3.9)

if(CMAKE_VERSION VERSION_GREATER_EQUAL 3.10)
  # HP-UX uses 3.9
  cmake_policy(VERSION 3.9...3.15)
  cmake_policy(SET CMP0075 OLD)
endif()

project(CML LANGUAGES C)

LIST(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake/modules")
set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH}" CACHE STRING "Modules for CMake" FORCE)

include(CustomTool)

add_subdirectory(simple-generator)
