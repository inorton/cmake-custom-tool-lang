#!/usr/bin/env python3
"""
Generate several source files and a header
"""
import os
import sys

folder = sys.argv[1]
prefix = sys.argv[2]

for name in range(1, 5):
    name = str(name)
    filename = "{}{}".format(prefix, name)
    with open(os.path.join(folder, filename + ".c"), "w") as sf:
        # noinspection PyStringFormat
        sf.write("""
        #include <stdio.h>
        #include \"""" + filename + """.h\"
        
        void """ + filename + """(void) {
          fprintf(stderr, "hello\\n");
        }
        """)

    with open(os.path.join(folder, filename + ".h"), "w") as sf:
        # noinspection PyStringFormat
        sf.write("""
        void {}(void);        
        """.format(filename))


